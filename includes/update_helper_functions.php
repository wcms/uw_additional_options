<?php

/**
 * @file
 * Functions to help updates.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\field\Entity\FieldConfig;

/**
 * Function to set the uw option field storage.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _uw_additional_options_set_option_field_storage() {

  // The field storage name.
  $name = 'field.storage.node.field_uw_option';

  // Get the path to cfg common config install directory.
  $path = \Drupal::service('extension.list.module')->getPath('uw_cfg_common') . '/config/install/';
  $config_dir = new FileStorage($path);

  // Get the config from the yml file into an array.
  $config_record = $config_dir->read($name);

  // Get the entity type.
  $entity_type = \Drupal::service('config.manager')
    ->getEntityTypeIdByName($name);

  // Get the storage.
  $storage = \Drupal::entityTypeManager()->getStorage($entity_type);

  // Try and create the new field and if not log that
  // it was already created.
  try {

    // Create the field.
    $field = $storage->createFromStorageRecord($config_record);

    // Save the new field.
    $field->save();
  }
  catch (Exception $e) {
    \Drupal::logger('uw_additional_options')->info('Field UW option already exists.');
  }
}

/**
 * Function to set the uw option field.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _uw_additional_options_set_uw_option_field() {

  // Get the content types to batch.
  $content_types = _uw_additional_options_get_node_types();

  // Step through each of the content types and install the fields,
  // and also setup default values for the fields.
  foreach ($content_types as $path => $content_type) {

    // Get the path to the content type.
    $path = \Drupal::service('extension.list.module')->getPath($path) . '/config/install/';
    $config_dir = new FileStorage($path);

    // Get the name of the field.
    $name = 'field.field.node.' . $content_type . '.field_uw_option';

    // Get the config from the yml into an array.
    $config_record = $config_dir->read($name);

    if ($config_record) {
      // If the field config is not install, install it.
      if (!FieldConfig::loadByName(
        $config_record['entity_type'],
        $config_record['bundle'],
        $config_record['field_name'])
      ) {
        FieldConfig::create($config_record)->save();
      }
    }
  }
}

/**
 * Function to get the uw content types.
 *
 * @param bool $web_page_not_install
 *   Whether or not to include web page in content types to not install.
 *
 * @return array
 *   The array of content types.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _uw_additional_options_get_node_types(bool $web_page_not_install = FALSE) {

  // The content types that are not getting any changes.
  $cts_not_to_install = [
    'uw_ct_sidebar',
    'uw_ct_site_footer',
    'uw_ct_expand_collapse_group',
  ];

  // If web pages are not to be install, add them to the list.
  if ($web_page_not_install) {
    $cts_not_to_install[] = 'uw_ct_web_page';
  }

  // Get all the node types.
  $node_types = \Drupal::entityTypeManager()
    ->getStorage('node_type')
    ->loadMultiple();

  // Step through each node type and get the path to
  // the module and the machine name.
  foreach ($node_types as $node_type) {

    // Get the id from the node type.
    $id = $node_type->id();

    // Ensure we are only getting the node type
    // that need updating.
    if (!in_array($id, $cts_not_to_install)) {

      // Catalogs and opportunities have different paths
      // and ids, so setup the content types array with
      // the correct info.
      if ($id === 'uw_ct_catalog_item') {
        $content_types['uw_ct_catalog'] = $id;
      }
      elseif ($id === 'uw_ct_opportunity') {
        $content_types['uw_ct_opportunities'] = $id;
      }
      else {
        $content_types[$id] = $id;
      }
    }
  }

  return $content_types;
}

/**
 * Function to set the exclude auto.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _uw_additional_options_set_exclude_auto_storage() {

  // The field storage name.
  $name = 'field.storage.node.field_uw_exclude_auto';

  // Get the path to cfg common config install directory.
  $path = \Drupal::service('extension.list.module')->getPath('uw_cfg_common') . '/config/install/';
  $config_dir = new FileStorage($path);

  // Get the config from the yml file into an array.
  $config_record = $config_dir->read($name);

  // Get the entity type.
  $entity_type = \Drupal::service('config.manager')
    ->getEntityTypeIdByName($name);

  // Get the storage.
  $storage = \Drupal::entityTypeManager()->getStorage($entity_type);

  // Try and create the new field and if not log that
  // it was already created.
  try {

    // Create the field.
    $field = $storage->createFromStorageRecord($config_record);

    // Save the new field.
    $field->save();
  }
  catch (Exception $e) {
    \Drupal::logger('uw_additional_options')->info('Field exclude auto already exists.');
  }
}

/**
 * Function to set the uw option field.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _uw_additional_options_set_exclude_auto_field() {

  // Get the content types to batch.
  $content_types = _uw_additional_options_get_node_types(TRUE);

  // Step through each of the content types and install the fields,
  // and also setup default values for the fields.
  foreach ($content_types as $path => $content_type) {

    // Get the path to the content type.
    $path = \Drupal::service('extension.list.module')->getPath($path) . '/config/install/';
    $config_dir = new FileStorage($path);

    // Get the name of the field.
    $name = 'field.field.node.' . $content_type . '.field_uw_exclude_auto';

    // Get the config from the yml into an array.
    $config_record = $config_dir->read($name);

    if ($config_record) {
      // If the field config is not install, install it.
      if (!FieldConfig::loadByName(
        $config_record['entity_type'],
        $config_record['bundle'],
        $config_record['field_name'])
      ) {
        FieldConfig::create($config_record)->save();
      }
    }
  }
}
