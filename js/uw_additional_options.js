/**
 * @file
 * Defines jQuery to provide summary information inside vertical tabs.
 */

(function ($) {

  'use strict';

  /**
   * Provide summary information for vertical tabs.
   */
  Drupal.behaviors.uw_additional_options_settings = {
    attach: function (context) {

      // Provide summary when editing a node.
      $('details#edit-uw-additional-options', context).drupalSetSummary(function (context) {

        var vals = [];

        var text = '';

        switch($('#edit-field-uw-option').val()) {
          case 'all':
            text += 'Show site title and menus';
            break;

          case 'title':
            text += 'Show site title only';
            break;

          case 'none':
            text += 'No site title or menus';
            break;
        }

        if ($('#edit-field-uw-exclude-auto-value').is(':checked')) {
          if (text.length > 0) {
            text += ', ';
          }
          text += 'exclude from automatically-generated listings';
        }

        vals.push(Drupal.t(text));

        return vals.join('<br/>');
      });
    }
  };

})(jQuery);
